use voicemod;

create table voicemod.flattened_joined_table
as
select 
	w.swid,
	w.timestamp,
	w.url,
	str_to_date(r.birth_dt, "%e-%b-%y") as birth_dt,
	r.gender_cd, 
    u.category,
	w.quoted_field_1,
	w.quoted_field_2,
	w.quoted_field_3,
	w.unquoted_field_0,
	w.unquoted_field_1,
	w.unquoted_field_10,
	w.unquoted_field_13,
	w.unquoted_field_14,
	w.unquoted_field_15,
	w.unquoted_field_16,
	w.unquoted_field_17,
	w.unquoted_field_18,
	w.unquoted_field_19,
	w.unquoted_field_2,
	w.unquoted_field_20,
	w.unquoted_field_21,
	w.unquoted_field_22,
	w.unquoted_field_23,
	w.unquoted_field_24,
	w.unquoted_field_25,
	w.unquoted_field_26,
	w.unquoted_field_27,
	w.unquoted_field_28,
	w.unquoted_field_29,
	w.unquoted_field_3,
	w.unquoted_field_30,
	w.unquoted_field_31,
	w.unquoted_field_32,
	w.unquoted_field_33,
	w.unquoted_field_34,
	w.unquoted_field_35,
	w.unquoted_field_36,
	w.unquoted_field_37,
	w.unquoted_field_38,
	w.unquoted_field_39,
	w.unquoted_field_4,
	w.unquoted_field_40,
	w.unquoted_field_41,
	w.unquoted_field_42,
	w.unquoted_field_43,
	w.unquoted_field_44,
	w.unquoted_field_45,
	w.unquoted_field_5,
	w.unquoted_field_6,
	w.unquoted_field_7,
	w.unquoted_field_8,
	w.unquoted_field_9,
	w.unquoted_field_46,
	w.unquoted_field_47,
	w.unquoted_field_48
from voicemod.web_log_raw w
left outer join voicemod.regusers_raw r on w.swid = r.swid
left outer join voicemod.url_map_raw u on w.url	= u.url;

-- What are the most popular categories that users visit? --> Clothing, Handbags, Home&Garden are the top 3 categories
select category, count(*) as ct
from voicemod.flattened_joined_table
group by category
order by ct desc;

-- What are the most popular categories by gender? --> Clothing is more popularly visited by females and electronics is more popular with males. 
select gender_cd, category, count(*) as ct
from voicemod.flattened_joined_table
group by gender_cd
order by ct desc;

-- What are the most popular categories by birth year on Tuesdays?
select extract(year from birth_dt) as birth_year, category, count(*) as ct
from voicemod.flattened_joined_table
where DAYNAME(birth_dt) = 'Tuesday'
group by birth_year
order by ct desc;











