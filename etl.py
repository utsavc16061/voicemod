import csv
import re
import pandas as pd
from sqlalchemy import create_engine

# Extracting credentials from .credentials file
with open(".credentials", "r") as cred_file:
    for line in cred_file:
        if "mysql_user" in line:
            user = line.split("=")[1].strip()
        elif "mysql_password" in line:
            pw = line.split("=")[1].strip()
        elif "mysql_db" in line:
            db = line.split("=")[1].strip()
        elif "mysql_hostname" in line:
            host = line.split("=")[1].strip()


# Create SQLAlchemy engine to connect to MySQL Database
engine = create_engine("mysql+pymysql://{user}:{pw}@{host}/{db}"
                .format(host=host, db=db, user=user, pw=pw))

####################### TRANSFORM #######################

# This function takes in file name and a comma separated list of column names. 
# It then parses the file using tab as the delimiter and double-quotes as identifier, 
# skips the first row of data and returns parsed data as a pandas dataframe
def parse_tsv(filename, col_names):
    output_list = []
    column_list = col_names.split(",")
    with open(filename) as fd:
        rd = csv.reader(fd, delimiter="\t", quotechar='"')
        next(rd) # skip header row
        for row in rd:
            output_list.append(row)
    output_df = pd.DataFrame(data=output_list, columns=column_list)
    return output_df
            
# This function takes in the file name, first parse the fields surrounded by double quotes and add to a dictionary, 
# remove the parsed fields from the raw string, and parse the remaining fields using space as the delimiter. 
# Based on the task, identified rows such as swid, url, etc. have been named accordingly. All other fields have either been named quoted or unquoted fields
# along with column index as per the raw data provided. Function returns parsed data in the form of pandas dataframe
def parse_log(filename):
    output = pd.DataFrame()
    with open(filename) as fd:
        for line in fd:
            clean_row = {}
            quoted_fields = re.findall(r'"(.*?)"', str(line)) #extracting any substring surrounded by double-quotes
            try:
                clean_row['timestamp'] = quoted_fields[0]
            except:
                clean_row['timestamp'] = ""
            try:
                clean_row['quoted_field_1'] = quoted_fields[1]
            except:
                clean_row['quoted_field_1'] = ""
            try:
                clean_row['quoted_field_2'] = quoted_fields[2]
            except:
                clean_row['quoted_field_2'] = ""
            try:
                clean_row['quoted_field_3'] = quoted_fields[3]
            except:
                clean_row['quoted_field_3'] = ""
            line = re.sub(r'"(.*?)"', "",str(line))
            other_fields = re.split(' +', line)
            for i in range(len(other_fields)):
                if i == 11:
                    clean_row['url'] = other_fields[i]
                elif i == 12:
                    clean_row['swid'] = other_fields[i].replace("{","").replace("}","")
                else:
                    clean_row['unquoted_field_'+str(i)] = other_fields[i] 
            output = output.append(clean_row, ignore_index=True)
    print(output.head())
    return output
    
####################### LOAD #######################
## web log
try:
    input_dataframe = parse_log("data/web_log_sample.log")
    input_dataframe.fillna(0, inplace=True)                                  
    input_dataframe.to_sql('web_log_raw', engine, index=False) # this will create table named web_log_raw in MySQL
except Exception as e:
    print(e)

## url map
try:
    url_map_dataframe = parse_tsv("data/urlmap.tsv","url,category")
    url_map_dataframe.fillna(0, inplace=True)
    url_map_dataframe.to_sql('url_map_raw', engine, index=False) # this will create table named url_map_raw in MySQL
except Exception as e:
    print(e)

## reg users
try:
    url_map_dataframe = parse_tsv("data/regusers.tsv","swid,birth_dt,gender_cd")
    url_map_dataframe.fillna(0, inplace=True)
    url_map_dataframe.to_sql('regusers_raw', engine, index=False) # this will create table named regusers_raw in MySQL
except Exception as e:
    print(e)





